/**
 * Created by Tobias on 03.04.2015.
 */
public class List {
    /**
     * Length of the List
     */
    private int length = 0;
    /**
     * Last Element of the List
     */
    private Element lastElement;
    /**
     * First Element of the List
     */
    private Element firstElement;
    /**
     * Element that is mostly somewhere between first and last Element
     */
    private Element middleElement;
    /**
     * index of the middleElement
     */
    private int middlePosition;
    /**
     * Element that is received from an other List
     */
    private Element shortStorage;

    /**
     * This Method adds a value to the List
     * @param value value that will be added
     */
    public void add(int value) {
        Element tmp;
        if (length == 0) {
            firstElement = new Element(value);
            lastElement = firstElement;
            middleElement = firstElement;
            middlePosition = 0;
            length++;
        } else if (length == 1) {
            lastElement = new Element(value);
            lastElement.setOneBeforebi(firstElement);
            length++;
        } else {
            tmp = new Element(value);
            tmp.setOneBeforebi(lastElement);
            tmp.setTwoBeforebi(lastElement.getOneBefore());
            lastElement = tmp;
            length++;
        }
    }

    /**
     * Method to read out the value at a given index
     * @param index index of which Element you would get the value
     * @return the value of the index
     */
    public int get(int index) {
        index = validateindex(index);
        if (index >= 0) {
            return walk(index).getValue();
        } else if (index < 0) {
            return walk(length + index).getValue();
        } else {
            throw new IndexOutOfBoundsException("Index not in Bound");
        }
    }

    /**
     * Method to set an Element
     * This Method is used from an other List to communicate which each other
     * @param tmp The Element that should be saved
     */
    public void setshortStorage(Element tmp) {
        shortStorage = tmp;
    }

    /**
     * This Method send the intern used Element to an other list
     * @param index is the index of the Element that will send
     * @param o must be instance of List
     * @throws WrongClassTypeException if the given Object is no instance of a List
     */
    public void sendElement(int index, Object o) throws WrongClassTypeException {
        index = validateindex(index);
        if (o instanceof List) {
            if ((index >= 0) && (index < length)) {
                ((List) o).setshortStorage(walk(index));
            } else if ((index < 0) && (index >= (length * -1))) {
                ((List) o).setshortStorage(walk(length + index));
            } else {
                throw new IndexOutOfBoundsException("Index out of Bound");
            }
        }
        else{
            throw new WrongClassTypeException("The given objekt was no List");
        }
    }

    /**
     * This Method calculates on what way it have to walk
     * @param index the index that should be reached
     * @return the Element of the index
     */
    private Element walk(int index) {
        int deltaStart = index;
        int deltaEnd = Math.abs(index - length) - 1;
        int deltaMiddle = middlePosition - index;
        int smallest = Math.min(Math.abs(deltaMiddle), Math.min(Math.abs(deltaEnd), deltaStart));

        if (smallest == deltaStart) {
            middleElement = walkPositive(deltaStart, firstElement);
            middlePosition = index;
            return middleElement;
        } else if (smallest == deltaEnd) {
            middleElement = walkNegative(deltaEnd, lastElement);
            middlePosition = index;
            return middleElement;
        } else if (Math.abs(deltaMiddle) == smallest) {
            if (deltaMiddle <= 0) {
                middleElement = walkPositive(Math.abs(deltaMiddle), middleElement);
                middlePosition = index;
                return middleElement;
            } else if (deltaMiddle > 0) {
                middleElement = walkNegative(deltaMiddle, middleElement);
                middlePosition = index;
                return middleElement;
            }
        }
        return null;
    }

    /**
     * Walk in positive way to come to the index
     * @param steps the steps which are needed to reach the index
     * @param start The Element on which it should start
     * @return the reached Element
     */
    private Element walkPositive(int steps, Element start) {
        Element tmp = start;
        int one= steps % 2;
        int two = (steps - one) / 2;
        for (; two != 0; two--) {
            tmp = tmp.getTwoAfter();
        }
        for (; one > 0; one--) {
            tmp = tmp.getOneAfter();
        }
        return tmp;
    }

    /**
     * Walk in negative way to come to the index
     * @param steps the steps which are needed to reach the index
     * @param start The Element on which it should start
     * @return the reached Element
     */
    private Element walkNegative(int steps, Element start) {
        Element tmp = start;
        int one = steps % 2;
        int two = (steps - one) / 2;
        for (; two != 0; two--) {
            tmp = tmp.getTwoBefore();
        }
        for (; one > 0; one--) {
            tmp = tmp.getOneBefore();
        }
        return tmp;
    }

    /**
     * This Method gives the length of the list.
     * @return length
     */
    public int length() {
        return length;
    }

    /**
     * clears the complete List
     */
    public void clear() {
        firstElement = null;
        lastElement = null;
        middleElement = null;
        middlePosition = 0;
        length = 0;
    }

    /**
     * This Method removes a Value from the List
     * @param index gives the index that should be deleted
     * @return true if it was possible or false if it wasn't
     */
    public boolean removeElement(int index) {
        index = validateindex(index);
        if ((index >= 2) && (index < (length - 2))) {
            Element delete = walk(index);
            delete.getOneBefore().setOneAfter(delete.getOneAfter());
            delete.getTwoBefore().setTwoAfter(delete.getTwoAfter());
            delete.getOneAfter().setOneBefore(delete.getOneBefore());
            delete.getTwoAfter().setTwoBefore(delete.getTwoBefore());
            length--;
            return true;
        } else if ((index == 0) && (length == 1)) {
            firstElement = null;
            lastElement = null;
            middleElement = null;
            middlePosition = -1;
            length--;
            return true;
        } else if ((index == 1) && (length == 2)) {
            firstElement = new Element(firstElement.getValue());
            length--;
            return true;
        } else if ((index == 0) && (length == 2)) {
            firstElement = new Element(lastElement.getValue());
            length--;
            return true;
        } else if (index == 0) {
            firstElement.getOneAfter().setOneBefore(null);
            firstElement.getTwoAfter().setTwoBefore(null);
            firstElement = firstElement.getOneAfter();
            length--;
            return true;
        } else if (index == 1) {
            Element tmp = firstElement.getOneAfter();
            tmp.getOneAfter().setOneBeforebi(firstElement);
            tmp.getOneAfter().setTwoBefore(null);
            tmp.getTwoAfter().setTwoBeforebi(firstElement);
            length--;
            return true;
        } else if (index == length - 1) {
            Element tmp = lastElement;
            lastElement = lastElement.getOneBefore();
            tmp.getOneBefore().setOneAfter(null);
            tmp.getTwoBefore().setTwoAfter(null);
            length--;
            return true;
        } else if (index == length - 2) {
            Element tmp = lastElement.getOneBefore();
            lastElement.setOneBefore(tmp.getOneBefore());
            tmp.getOneBefore().setOneAfter(lastElement);
            tmp.getOneBefore().setTwoAfter(null);
            tmp.getTwoBefore().setTwoAfter(lastElement);
            length--;
            return true;
        }
        return false;
    }

    /**
     * replace a old value at the given index with a new value
     * @param value this is the value which will be changed
     * @param index the index gives the position where the value will be changed
     */
    public void replace(int value, int index) {
        index = validateindex(index);
        walk(index).setValue(value);
    }

    /**
     * Paste a value at the position of the index
     * @param index is the position where you would paste your Element
     */
    public void paste(int index) {
        if (Math.abs(index) != length) {
            index = validateindex(index);
            if ((index >= 2) && (index < (length - 1))) {
                Element previous = walk(index - 1);
                Element past = walk(index);
                shortStorage.setOneBeforebi(previous);
                past.setOneBeforebi(shortStorage);
                past.setTwoBeforebi(previous);
                shortStorage.setTwoBeforebi(previous.getOneBefore());
                past.getOneAfter().setTwoBeforebi(shortStorage);
                length++;
            } else if ((index == 0) && (length == 1)) {
                firstElement.setOneBeforebi(shortStorage);
                firstElement = shortStorage;
                if (middlePosition == 0) {
                    middleElement = shortStorage;
                }
                length++;
            } else if ((index == 1) && (length == 2)) {
                firstElement.getOneAfter().setTwoBeforebi(firstElement);
                shortStorage.setOneBeforebi(firstElement);
                firstElement.getTwoAfter().setOneBeforebi(shortStorage);
                length++;
            } else if ((index == 0) && (length == 2)) {
                firstElement.setOneBeforebi(shortStorage);
                firstElement.getOneAfter().setTwoBeforebi(shortStorage);
                firstElement = shortStorage;
                length++;
            } else if (index == 0) {
                firstElement.setOneBeforebi(shortStorage);
                firstElement.getOneAfter().setTwoBeforebi(shortStorage);
                firstElement = shortStorage;
                length++;
            } else if (index == 1) {
                firstElement.getOneAfter().setTwoBeforebi(firstElement);
                shortStorage.setOneBeforebi(firstElement);
                firstElement.getTwoAfter().setOneBeforebi(shortStorage);
                firstElement.getTwoAfter().getOneAfter().setTwoBeforebi(shortStorage);
                length++;
            } else if (index == length - 1) {
                shortStorage.setOneBeforebi(lastElement.getOneBefore());
                shortStorage.setTwoBeforebi(lastElement.getTwoBefore());
                lastElement.setTwoBeforebi(lastElement.getOneBefore());
                lastElement.setOneBeforebi(shortStorage);
                length++;
            }
        } else if ((Math.abs(index) == length) & (length != 0)) {
            if (length >= 1) {
                shortStorage.setOneBeforebi(lastElement);
                lastElement = shortStorage;
            }
            if (lastElement.getOneBefore() != null) {
                shortStorage.setTwoBeforebi(lastElement.getOneBefore());
                lastElement = shortStorage;
            }
            lastElement = shortStorage;
            length++;
        } else if (length == 0) {
            add(shortStorage.getValue());
        } else {
            throw new IndexOutOfBoundsException("Index not in Bound");
        }
    }

    /**
     * Checks if the given index is valid to read out
     * @param index index that should be checked
     * @return a positiv index if the index is in bound
     */
    private int validateindex(int index) {
        index = Math.abs(index);
        if ((index >= 0) && (index < length)) {
            return index;
        } else {
            throw new IndexOutOfBoundsException("Index not in Bound");
        }
    }


    /**
     * Class of the Elements that are managed by the List Class.
     */
    private class Element {
        private int value;
        private Element oneBefore;
        private Element twoBefore;
        private Element oneAfter;
        private Element twoAfter;

        public Element(int value) {
            this.value = value;
        }

        public Element() {
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Element getOneBefore() {
            return oneBefore;
        }

        public void setOneBefore(Element oneBefore) {
            this.oneBefore = oneBefore;
        }

        public void setOneBeforebi(Element oneBefore) {
            this.oneBefore = oneBefore;
            oneBefore.setOneAfter(this);
        }

        public Element getTwoBefore() {
            return twoBefore;
        }

        public void setTwoBefore(Element twoBefore) {
            this.twoBefore = twoBefore;
        }

        public void setTwoBeforebi(Element twoBefore) {
            this.twoBefore = twoBefore;
            twoBefore.setTwoAfter(this);
        }

        public Element getOneAfter() {
            return oneAfter;
        }

        public void setOneAfter(Element oneAfter) {
            this.oneAfter = oneAfter;
        }

        public Element getTwoAfter() {
            return twoAfter;
        }

        public void setTwoAfter(Element twoAfter) {
            this.twoAfter = twoAfter;
        }
    }
}
