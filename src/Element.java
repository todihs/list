/**
 * Created by Tobias on 03.04.2015.
 */
public class Element {
    private int value;
    private Element oneBefore;
    private Element twoBefore;
    private Element oneAfter;
    private Element twoAfter;

    private Element(int value) {
        this.value = value;
    }

    private Element() {

    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Element getOneBefore() {
        return oneBefore;
    }

    public void setOneBefore(Element oneBefore) {
        this.oneBefore = oneBefore;
    }

    public void setOneBeforebi(Element oneBefore) {
        this.oneBefore = oneBefore;
        oneBefore.setOneAfter(this);
    }

    public Element getTwoBefore() {
        return twoBefore;
    }

    public void setTwoBefore(Element twoBefore) {
        this.twoBefore = twoBefore;
    }

    public void setTwoBeforebi(Element twoBefore) {
        this.twoBefore = twoBefore;
        twoBefore.setTwoAfter(this);
    }

    public Element getOneAfter() {
        return oneAfter;
    }

    public void setOneAfter(Element oneAfter) {
        this.oneAfter = oneAfter;
    }

    public Element getTwoAfter() {
        return twoAfter;
    }

    public void setTwoAfter(Element twoAfter) {
        this.twoAfter = twoAfter;
    }


}
