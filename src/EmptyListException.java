/**
 * Created by Tobias on 05.05.2015.
 */
public class EmptyListException extends Exception {
    public EmptyListException(String s) {
        super(s);
    }
}
