/**
 * Created by Tobias on 06.05.2015.
 */
public class WrongClassTypeException extends Exception {
    public WrongClassTypeException(String s) {
        super(s);
    }
}
